#!/bin/bash
until java -jar restaurant-1.0.jar 
do
  echo "Restaurant crashed with exit code $?.  Respawning.." >&2
  sleep 1
done
