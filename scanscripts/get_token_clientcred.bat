curl -X POST -sS ^
  -H "Content-type: application/x-www-form-urlencoded" ^
  -H "Accept: application/json" ^
  -H "Authorization: Basic cmVzdGF1cmFudDpmMGFkNzUyYy00YmU3LTQ4MTktYjA2Mi1kNGI1NTA1N2M4MmU=" ^
  -d "grant_type=client_credentials" ^
  https://keycloak.widemo.xyz/auth/realms/demorealm/protocol/openid-connect/token ^
  | jq -r .access_token ^
