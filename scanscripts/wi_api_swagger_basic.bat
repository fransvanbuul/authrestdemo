curl -sS -X POST ^
  -H "Content-type: application/json" ^
  -H "Accept: application/json" ^
  -d "{ \"apiDefinitionUrl\": \"https://restaurant.widemo.xyz/v2/api-docs\", \"apiType\": \"swagger\", \"outputName\": \"RESTaurant API-started Swagger Basic\", \"authCredentials\": \"Basic dXNlcjE6cHdk\" }" ^
  http://localhost:8083/webinspect/APIScanner/OpenAPI/startAPIScan
