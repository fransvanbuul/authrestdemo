curl -X POST -sS ^
  -H "Content-type: application/x-www-form-urlencoded" ^
  -H "Accept: application/json" ^
  -H "Authorization: Basic cmVzdGF1cmFudDpmMGFkNzUyYy00YmU3LTQ4MTktYjA2Mi1kNGI1NTA1N2M4MmU=" ^
  -d "grant_type=password&username=user1&password=pwd" ^
  https://keycloak.widemo.xyz/auth/realms/demorealm/protocol/openid-connect/token ^
  | jq -r .access_token ^
  >token.tmp

@for /f "delims=" %%x in (token.tmp) do @set TOKEN=%%x
curl -sS -X POST ^
  -H "Content-type: application/json" ^
  -H "Accept: application/json" ^
  -d "{ \"apiDefinitionUrl\": \"https://restaurant.widemo.xyz/v2/api-docs\", \"apiType\": \"swagger\", \"outputName\": \"RESTaurant API-started Swagger OAuth2 Static\", \"authCredentials\": \"Bearer %TOKEN%\" }" ^
  http://localhost:8083/webinspect/APIScanner/OpenAPI/startAPIScan

@erase *.tmp
