jq ".auth.basic = [ { key: \"username\", value: \"user1\", type: \"string\" }, { key: \"password\", value: \"pwd\", type: \"string\" } ]"  ^
   restaurant_postman_basic_no_hardcoded_pwd.json ^
   >restaurant_postman_basic_no_hardcoded_pwd.tmp.json

wi.exe -v -n "RESTaurant CLI-started Postman Basic" ^
  -ah https://restaurant.widemo.xyz ^
  -ps 1 ^
  -pwc restaurant_postman_basic_no_hardcoded_pwd.tmp.json

@erase *.tmp *.tmp.json
