wi.exe -v -n "RESTaurant CLI-started Postman OAuth2 Dynamic - Option 2" ^
  -ah https://restaurant.widemo.xyz ^
  -ps 1 ^
  -pdac ^
  -plc restaurant_postman_gettoken.json ^
  -pwc restaurant_postman_bearer_dynamic.json ^
  -rs @BearerResponseStateRule.txt ^
  -pls [STATUSCODE]401
